var Twit = require('twitter'),
    settings = require('./lib/settings.js'),
    fs = require('fs');

var T = new Twit({
    consumer_key:         settings.consumer_key
  , consumer_secret:      settings.consumer_secret
  , access_token_key:     settings.access_token
  , access_token_secret:  settings.access_token_secret
});


var Utility = {
  a : ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '],

  b : ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'],

  inWords : function(num) {
    if ((num = num.toString()).length > 9) return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return; var str = '';
    str += (n[1] != 0) ? (Utility.a[Number(n[1])] || Utility.b[n[1][0]] + ' ' + Utility.a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (Utility.a[Number(n[2])] || Utility.b[n[2][0]] + ' ' + Utility.a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (Utility.a[Number(n[3])] || Utility.b[n[3][0]] + ' ' + Utility.a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (Utility.a[Number(n[4])] || Utility.b[n[4][0]] + ' ' + Utility.a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? '' : '') + (Utility.a[Number(n[5])] || Utility.b[n[5][0]] + ' ' + Utility.a[n[5][1]]): '';
    return str;
  },

  randomInt : function(min,max) {
    return Math.floor(Math.random()*(max-min+1)+min);
  },

  toTitleCase : function(str) {
    str = str.substring(0, str.length - 1);
    return str.charAt(0).toUpperCase() + str.slice(1);
  },

  setVariableInterval : function(callbackFunc, timing) {
    var variableInterval = {
      interval: timing,
      callback: callbackFunc,
      stopped: false,
      runLoop: function() {
        if (variableInterval.stopped) return;
        var result = variableInterval.callback.call(variableInterval);
        if (typeof result == 'number')
        {
          if (result === 0) return;
          variableInterval.interval = result;
        }
        variableInterval.loop();
      },
      stop: function() {
        this.stopped = true;
        clearTimeout(this.timeout);
      },
      start: function() {
        this.stopped = false;
        return this.loop();
      },
      loop: function() {
        this.timeout = setTimeout(this.runLoop, this.interval);
        return this;
      }
    };
    return variableInterval.start();
  }

}

var Tweet = {

  tweet: function(number) {
    status = Utility.toTitleCase(number) + '!';
    

    shouldWeLaugh = Utility.randomInt(1, 8);
    if(shouldWeLaugh === 4) {
      status = status + ' Ah ah ah!'
    }
    T.post('statuses/update', {status: status},  function(error, tweet, response){
       if(error) throw error;
    });
  }

}

Utility.setVariableInterval(function() {

  var date = new Date();
  var current_hour = date.getHours();

  if(current_hour >= 8 && current_hour <= 22) {

    var number = fs.readFileSync('num.txt', 'utf8');
    number = parseInt(number) + 1;

    Tweet.tweet(Utility.inWords(number));

    fs.writeFile('num.txt', number, 'utf8')

  }

  

  increment = Utility.randomInt(2, 12) * 3600000;


  return increment;

}, 100);